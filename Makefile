build:
	docker-compose build
	docker-compose up -d

install:
	docker exec -it php composer install
	docker exec -it php cp .env.example .env
	docker exec -it php php artisan key:generate
	docker exec -it php php artisan migrate:fresh
	docker exec -it php php artisan db:seed

tests:
	docker exec -it php php artisan test

logs:
	docker exec -it php tail -f -n 0 storage/logs/laravel.log
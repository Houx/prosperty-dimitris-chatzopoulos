## Set up
* `make build`
* `make install`

## Postman
* Open `Prosperty - Assigment.postman_collection.json`
* Make AUTH request, get token and add it to global variables

## For Test Run
* `make tests`

## What would I improve?
    # Tests
        * A new .env.testing environment with a new database only for test.
        * More tests, validation and Unit test for attributes and small function in project
    # Sort & Filter
        * In this case i will make a better way to response data. Something like [strategy](https://refactoring.guru/design-patterns/strategy) or [Decorator](https://refactoring.guru/design-patterns/decorator) and Stubs for filters and sorts
        * I prefer to use a design pattern in that case to avoide add the logic in Service
    # Validation
        * More validation and handle response error to add better message.
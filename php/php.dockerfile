FROM php:8.1-fpm-alpine 

RUN mkdir -p /var/www/html

RUN apk update

RUN apk add jpeg-dev libpng-dev zlib-dev

RUN apk add --no-cache pcre-dev $PHPIZE_DEPS \
&& pecl install redis \
&& docker-php-ext-enable redis.so

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN docker-php-ext-install pdo pdo_mysql

RUN docker-php-ext-install gd

RUN docker-php-ext-enable gd

RUN mv /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini

<?php

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;

Route::get('/', static fn() => abort(Response::HTTP_NOT_FOUND));

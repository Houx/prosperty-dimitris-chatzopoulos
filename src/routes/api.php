<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Spy\SpyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->prefix('spy')->group(function () {
   Route::post('/', [SpyController::class, 'store']);

   Route::middleware(['throttle:request-spies'])
       ->get('random', [SpyController::class, 'random']);

   Route::get('/all', [SpyController::class, 'all']);
});

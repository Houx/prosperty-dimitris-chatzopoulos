<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('covert_agents', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('surname')->unique();
            $table->string('agency')->nullable();
            $table->string('country')->nullable();
            $table->date('birth_date');
            $table->date('death_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('covert_agents');
    }
};

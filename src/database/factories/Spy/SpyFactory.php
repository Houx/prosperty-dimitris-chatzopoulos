<?php

namespace Database\Factories\Spy;

use App\Enums\AgencyEnum;
use App\Models\Spy\Spy;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Spy\Spy>
 */
class SpyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $birthDate = fake()->date;

        return [
            Spy::ATTRIBUTE_NAME => fake()->unique()->firstName,
            Spy::ATTRIBUTE_SURNAME => fake()->unique()->lastName,
            Spy::ATTRIBUTE_AGENCY  => Arr::random(AgencyEnum::cases()),
            Spy::ATTRIBUTE_COUNTRY => fake()->countryCode,
            Spy::ATTRIBUTE_BIRTH_DATE => $birthDate,
            Spy::ATTRIBUTE_DEATH_DATE => fake()->dateTimeBetween($birthDate),
        ];
    }
}

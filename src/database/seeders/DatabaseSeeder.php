<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Spy\Spy;
use App\Models\User\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         Spy::factory(200)->create();

         User::factory()->create([
             'name' => 'Luke',
             'email' => 'skuwalker@starwars.com',
         ]);
    }
}

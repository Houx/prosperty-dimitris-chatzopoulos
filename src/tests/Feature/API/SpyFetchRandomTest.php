<?php

namespace Tests\Feature\API;

use App\Enums\PermissionEnum;
use App\Models\User\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class SpyFetchRandomTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     */
    public function testRateLimit(): void
    {
        $this->getAuthToken();

        for ($i = 1; $i <= 10; $i++) {
            $this->get('/api/spy/random')
                ->assertOk()
                ->assertHeader('X-Ratelimit-Remaining', 10 - $i);
        }
            $this->get('/api/spy/random')
                ->assertStatus(429)
                ->assertHeader('Retry-After', 60);
    }

    public function testRandomFetch()
    {
        $this->getAuthToken();

        $response = $this->get('/api/spy/random');
        $response->assertStatus(Response::HTTP_OK);

        $this->assertSame(5, count($response->json()));
    }


    private function getAuthToken(): Authenticatable
    {
        return Sanctum::actingAs(
            User::factory()->create(),
            [PermissionEnum::STORE->value]
        );
    }
}

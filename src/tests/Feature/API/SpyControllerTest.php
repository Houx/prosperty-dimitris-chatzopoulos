<?php

namespace Tests\Feature\API;

use App\Enums\PermissionEnum;
use App\Models\Spy\Spy;
use App\Models\User\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class SpyControllerTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     */
    public function testCreateSpy(): void
    {
        $this->getAuthToken();

        $attributes = [
            Spy::ATTRIBUTE_NAME       => 'R2D2',
            Spy::ATTRIBUTE_SURNAME    => 'C3PO',
            Spy::ATTRIBUTE_AGENCY     => 'EYP',
            Spy::ATTRIBUTE_COUNTRY    => 'GR',
            Spy::ATTRIBUTE_BIRTH_DATE => '1995-05-23',
            Spy::ATTRIBUTE_DEATH_DATE => '2023-10-10'
        ];

        $response = $this->json('post','/api/spy', $attributes);

        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJsonFragment([
            Spy::ATTRIBUTE_FULL_NAME => 'R2D2 C3PO',
            Spy::ATTRIBUTE_AGE       => 28
        ]);

        $this->assertDatabaseHas(Spy::TABLE, [
                Spy::ATTRIBUTE_NAME       => 'R2D2',
                Spy::ATTRIBUTE_SURNAME    => 'C3PO',
                Spy::ATTRIBUTE_AGENCY     => 'EYP',
                Spy::ATTRIBUTE_COUNTRY    => 'GR',
                Spy::ATTRIBUTE_BIRTH_DATE => '1995-05-23',
                Spy::ATTRIBUTE_DEATH_DATE => '2023-10-10',
            ]);
    }

    public function testValidationError(): void
    {
        $this->getAuthToken();

        $attributes = [
            Spy::ATTRIBUTE_NAME       => 'R2D2',
            Spy::ATTRIBUTE_SURNAME    => 'C3PO',
            Spy::ATTRIBUTE_AGENCY     => 'RANDOM_AGENCY',
            Spy::ATTRIBUTE_COUNTRY    => 'GR',
            Spy::ATTRIBUTE_BIRTH_DATE => '1995-05-23',
            Spy::ATTRIBUTE_DEATH_DATE => '2023-10-10'
        ];

        $response = $this->json('post','/api/spy', $attributes);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonFragment([
           'message' => 'The selected agency is invalid.'
        ]);

    }

    private function getAuthToken(): Authenticatable
    {
        return Sanctum::actingAs(
            User::factory()->create(),
            [PermissionEnum::STORE->value]
        );
    }
}

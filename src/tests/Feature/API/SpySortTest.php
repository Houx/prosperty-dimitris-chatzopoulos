<?php

namespace Tests\Feature\API;

use App\Enums\PermissionEnum;
use App\Models\Spy\Spy;
use App\Models\User\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class SpySortTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     */
    public function testPaginator(): void
    {
        $this->getAuthToken();

        $response = $this->get('/api/spy/all?sort[]=birth_date');

        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee('next');
        $this->assertSame(15, count($response->json('data')));
    }

    private function getAuthToken(): Authenticatable
    {
        return Sanctum::actingAs(
            User::factory()->create(),
            [PermissionEnum::STORE->value]
        );
    }
}

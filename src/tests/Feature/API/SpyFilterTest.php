<?php

namespace Tests\Feature\API;

use App\Enums\PermissionEnum;
use App\Models\Spy\Spy;
use App\Models\User\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class SpyFilterTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     */
    public function testFilter(): void
    {
        $this->getAuthToken();

        foreach(['23','24','25'] as $age) {
            Spy::factory()->create([
                Spy::ATTRIBUTE_BIRTH_DATE => now()->subYears($age),
                Spy::ATTRIBUTE_DEATH_DATE => now(),
                Spy::ATTRIBUTE_NAME       => 'John_'.$age,
                Spy::ATTRIBUTE_SURNAME    => 'Doe'.$age
            ]);
        }

        $response = $this->get('/api/spy/all?filter[age]=23');
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
           Spy::ATTRIBUTE_AGE => 23
        ]);

        $response = $this->get('/api/spy/all?filter[age]=23&filter[name]=John_23');
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
            Spy::ATTRIBUTE_AGE  => 23,
            Spy::ATTRIBUTE_NAME => 'John_23'
        ]);

        $this->assertSame(1, count($response->json('data')));
    }


    private function getAuthToken(): Authenticatable
    {
        return Sanctum::actingAs(
            User::factory()->create(),
            [PermissionEnum::STORE->value]
        );
    }
}

<?php

namespace App\Models\Spy;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Spy extends SpyBase
{
    use HasFactory;

    protected $table = self::TABLE;

    protected $fillable = [
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_SURNAME,
        self::ATTRIBUTE_AGENCY,
        self::ATTRIBUTE_COUNTRY,
        self::ATTRIBUTE_BIRTH_DATE,
        self::ATTRIBUTE_DEATH_DATE,
    ];

    protected $appends = [
        self::ATTRIBUTE_FULL_NAME,
        self::ATTRIBUTE_AGE
    ];

    protected function fullName(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->{self::ATTRIBUTE_NAME}." ".$this->{self::ATTRIBUTE_SURNAME},
        );
    }

    protected function age(): Attribute
    {
        return Attribute::make(
            get: fn () => Carbon::parse($this->{self::ATTRIBUTE_BIRTH_DATE})->diffInYears($this->{self::ATTRIBUTE_DEATH_DATE} ?? now()),
        );
    }

    public function scopeAgeExact(Builder $query, int $age): Builder
    {
        return $query->whereRaw(
            sprintf(
                'TIMESTAMPDIFF(YEAR, %s, %s) = %d',
                self::ATTRIBUTE_BIRTH_DATE,
                self::ATTRIBUTE_DEATH_DATE ?? 'NOW()',
                $age,
            ),
        );
    }

    public function scopeAgeRange(Builder $query, int $minAge, int $maxAge): Builder
    {
        return $query->whereRaw(
            sprintf(
                'TIMESTAMPDIFF(YEAR, %s, %s) BETWEEN %d AND %d',
                self::ATTRIBUTE_BIRTH_DATE,
                self::ATTRIBUTE_DEATH_DATE ?? 'NOW()',
                $minAge,
                $maxAge,
            ),
        );
    }
}

<?php

namespace App\Models\Spy;

use Illuminate\Database\Eloquent\Model;

class SpyBase extends Model
{
    public const TABLE = 'covert_agents';
    public const ATTRIBUTE_NAME = 'name';
    public const ATTRIBUTE_SURNAME = 'surname';
    public const ATTRIBUTE_AGENCY = 'agency';
    public const ATTRIBUTE_COUNTRY = 'country';
    public const ATTRIBUTE_BIRTH_DATE = 'birth_date';
    public const ATTRIBUTE_DEATH_DATE = 'death_date';
    public const ATTRIBUTE_FULL_NAME = 'full_name';
    public const ATTRIBUTE_AGE = 'age';


    public const FILTERS = [
      self::ATTRIBUTE_AGE,
      self::ATTRIBUTE_NAME,
      self::ATTRIBUTE_SURNAME
    ];

    public const SORTS = [
        Spy::ATTRIBUTE_FULL_NAME,
        Spy::ATTRIBUTE_DEATH_DATE,
        Spy::ATTRIBUTE_BIRTH_DATE,
    ];

}

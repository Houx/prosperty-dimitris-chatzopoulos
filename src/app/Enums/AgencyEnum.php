<?php

namespace App\Enums;

enum AgencyEnum: string
{
    case AGENCY_FBI = 'FBI';
    case AGENCY_NSA = 'NSA';
    case AGENCY_EYP = 'EYP';
    case AGENCY_MI6 = 'MI6';
    case AGENCY_FSB = 'FSB';
    case AGENCY_ISI = 'ISI';
    case AGENCY_CIA = 'CIA';
}

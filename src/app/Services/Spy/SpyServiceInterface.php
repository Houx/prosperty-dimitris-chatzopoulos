<?php

namespace App\Services\Spy;

use App\Models\Spy\Spy;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface SpyServiceInterface
{
    public function create(array $attributes): Model;
    public function getRandom(): Collection;
    public function getAllSpies(array $attributes): LengthAwarePaginator;
}

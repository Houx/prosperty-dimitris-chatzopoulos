<?php

namespace App\Services\Spy;

use App\Models\Spy\Spy;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;

class SpyService implements SpyServiceInterface
{
    public function create(array $attributes): Model
    {
        return Spy::query()->create($attributes);
    }

    public function getRandom(): Collection
    {
        return Spy::query()->inRandomOrder()->limit(5)->get();
    }

    public function getAllSpies(array $attributes): LengthAwarePaginator
    {
        $query = Spy::query();

        $query = $this->applyFilters($query, $attributes);
        $query = $this->applySorts($query, $attributes);

        return $query
            ->paginate(15);
    }

    private function applyFilters(Builder $query, $attributes): Builder
    {
        foreach ($attributes['filter'] ?? [] as $key => $filter) {
            $query = match ($key) {
                default            => $query->where($key, $filter),
                Spy::ATTRIBUTE_AGE => $this->applyAgeFilter($query, $filter),
            };
        }

        return $query;
    }

    private function applySorts(Builder $query, array $attributes): Builder
    {
        foreach ($attributes['sort'] ?? [] as $sortAttribute) {
            $query = match ($sortAttribute) {
                default                  => $query->orderBy($sortAttribute),
                Spy::ATTRIBUTE_FULL_NAME => $query
                    ->orderBy(Spy::ATTRIBUTE_NAME)
                    ->orderBy(Spy::ATTRIBUTE_SURNAME)
            };
        }

        return $query;
    }

    private function applyAgeFilter(Builder $query, string $filter): Builder
    {
        [$isAgeExact, $minAge, $maxAge] = $this->getAgeFilter($filter);

        $isAgeExact
            ? $query->ageExact((int) $filter)
            : $query->ageRange((int) $minAge, (int) $maxAge);

        return $query;
    }

    private function getAgeFilter(string $filter): array
    {
        $values = explode('-', $filter);
        if (count($values) === 2) {
            return [false, $values[0], $values[1]];
        }

        return [true, '', ''];
    }
}

<?php

namespace App\Http\Controllers\Spy;

use App\Http\Controllers\Controller;
use App\Http\Requests\Spy\SpyFilterRequest;
use App\Http\Requests\Spy\SpyStoreRequest;
use App\Models\Spy\Spy;
use App\Services\Spy\SpyServiceInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class SpyController extends Controller
{
    public function __construct(private SpyServiceInterface $spyService)
    {
    }

    public function all(SpyFilterRequest $request): LengthAwarePaginator
    {
        return $this->spyService->getAllSpies($request->safe()->toArray());
    }

    public function store(SpyStoreRequest $request): Model
    {
        return $this->spyService->create($request->safe()->toArray());
    }

    public function random(): Collection
    {
        return $this->spyService->getRandom();
    }
}

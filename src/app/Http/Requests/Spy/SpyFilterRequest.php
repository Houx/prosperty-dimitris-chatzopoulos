<?php

namespace App\Http\Requests\Spy;

use App\Enums\PermissionEnum;
use App\Models\Spy\Spy;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SpyFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(Request $request): bool
    {
        return $request->user()->tokenCan(PermissionEnum::STORE->value);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'sort'      => ['sometimes', 'array'],
            'sort.*'    => ['string', Rule::in($this->allowedSortAttributes())],
            'filter'    => ['sometimes','array', function ($attribute, $value, $fail) {
                $allowedAttributes = $this->allowedSearchAttributes();
                $invalidAttributes = array_diff(array_keys($value), $allowedAttributes);

                if (!empty($invalidAttributes)) {
                    $invalidAttributesList = implode(', ', $invalidAttributes);
                    $fail("$invalidAttributesList not allowed in the filter.");
                }
            }],
            'filter.*'  => ['string']
        ];
    }

    private function allowedSearchAttributes(): array
    {
        return Spy::FILTERS;
    }
    private function allowedSortAttributes(): array
    {
        return Spy::SORTS;
    }
}

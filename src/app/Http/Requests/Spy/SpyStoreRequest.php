<?php

namespace App\Http\Requests\Spy;

use App\Enums\AgencyEnum;
use App\Enums\PermissionEnum;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class SpyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(Request $request): bool
    {
        return $request->user()->tokenCan(PermissionEnum::STORE->value);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name'        => ['required', 'string', 'unique:covert_agents'],
            'surname'     => ['required', 'string', 'unique:covert_agents'],
            'agency'      => ['nullable', 'string', new Enum(AgencyEnum::class)],
            'country'     => ['required', 'string'],
            'birth_date'  => ['required', 'date', 'before_or_equal:today'],
            'death_date'  => ['nullable', 'date', 'after:birthDate', 'before_or_equal:today'],
        ];
    }
}
